===============
Data Popularity
===============

Whenever production data is accessed by a user job, we keep that information in order to estimate the popularity of the dataset. This is useful for various purposes:
 * Freeing disk space
 * Making data set consistent in terms of number of replicas
 * Produce plots for the RRB




.. toctree::
   :maxdepth: 2

   components.rst
   popularityCSV.rst
   popularityAnalysis.rst
